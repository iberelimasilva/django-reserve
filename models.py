# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Q


class ReserveUnavailableException(Exception):
	pass



class Resource(models.Model):
	name = models.CharField(max_length=100)

	"""
		Check if exist conflict on this resource on database

	"""
	def check_availability(self, date_in, date_out):

		# check that the dates are equal
		if date_in == date_out:
			return False

		reserve_count = self.reserve_set.filter(
			Q(date_in__gte=date_in) & Q(date_in__lte=date_out) |
            Q(date_out__gt=date_in) & Q(date_out__lte=date_out),
            resource=self).count()

		if reserve_count > 0:
			return False

		return True




	"""
		Save the reservation on database if is availability
	"""
	def make_reservation(self, date_in, date_out):
		if self.check_availability(date_in, date_out):
			return self.reserve_set.create(date_in=date_in, date_out=date_out)
		raise ReserveUnavailableException("Resource unavailable at this range time")



	def __unicode__(self):
		return self.name





class Reserve(models.Model):
	resource = models.ForeignKey(Resource)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	date_in = models.DateTimeField()
	date_out = models.DateTimeField()
	note = models.TextField(blank=True)

	def __unicode__(self):
		return u"(#%s) %s in:%s out:%s" % (self.id, self.resource, self.date_in, self.date_out)



