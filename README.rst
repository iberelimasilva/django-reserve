=====
django-reserve
=====

Django-Reserve is a Django app that helps the resource reservation management.
For each question, visitors can choose between a fixed number of answers.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "reserve" to your INSTALLED_APPS setting like this::

      INSTALLED_APPS = (
          ...
          'reserve',
      )


2. Create your resource objects to make reservations

	from reserve.models import *
	from datetime import datetime

	# create any resources
	resource_1 = Resource.objects.create(name="Room 1")
	resource_2 = Resource.objects.create(name="Room 2")

	# make reservations on resource number one at 01/01/2014 10:00:00 to 01/01/2014 20:00:00
	resource_1_date_in = datetime.strptime("01/01/2014 10:00:00", "%d/%m/%Y %H:%M:%S")
	resource_1_date_out = datetime.strptime("01/01/2014 20:00:00", "%d/%m/%Y %H:%M:%S")

	# check availability of resource and make the reservation
	reservation_1 = resource_1.make_reservation(resource_1_date_in, resource_1_date_out)

	# if try make reservation again your throw a unavailable exception
	reservation_2 = resource_1.make_reservation(resource_1_date_in, resource_1_date_out)



