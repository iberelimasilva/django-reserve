# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reserve', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reserve',
            old_name='date_registered',
            new_name='created_at',
        ),
        migrations.RenameField(
            model_name='reserve',
            old_name='date_updated',
            new_name='updated_at',
        ),
    ]
