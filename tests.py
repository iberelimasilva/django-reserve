from django.test import TestCase
from reserve.models import *
from datetime import datetime



class ReservationTestCase(TestCase):

	def setUp(self):
		

		# create any resource to make reservations
		self.resource_1 = Resource.objects.create(name="Resource 1")
		self.resource_2 = Resource.objects.create(name="Resource 2")
		self.resource_3 = Resource.objects.create(name="Resource 3")


	def test_check_tree_resources_on_database(self):

		# check if have tree resources on database
		self.assertEquals(Resource.objects.count(), 3)



	def test_if_exist_any_reserve_on_resources(self):

		# check if have zero reserve on database
		self.assertEquals(Reserve.objects.count(), 0)



	def test_create_a_reserve_with_same_time(self):

		# create two dates with same time
		date_in = datetime.strptime("01/01/2014 10:00:00", "%d/%m/%Y %H:%M:%S")
		date_out = datetime.strptime("01/01/2014 10:00:00", "%d/%m/%Y %H:%M:%S")

		# make the reserve fail on resource 1
		reserve = None
		try: 
			reserve = self.resource_1.make_reservation(date_in, date_out)
		except:
			pass

		self.assertEquals(reserve, None)



	def test_create_a_valid_reserve(self):

		# create two dates with range time
		date_in = datetime.strptime("01/01/2014 10:00:00", "%d/%m/%Y %H:%M:%S")
		date_out = datetime.strptime("01/01/2014 18:00:00", "%d/%m/%Y %H:%M:%S")

		# make the reserve success on resource 1
		reserve = self.resource_1.make_reservation(date_in, date_out)

		# check if the reserva was created
		self.assertEquals(Reserve.objects.count(), 1)

		# check collision on new reserve with this time
		#self.assertRaises(ReserveUnavailableException, self.resource_1.make_reservation(date_in, date_out))